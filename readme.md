This is a template for Spark based application along with unit test

```
-- | Project
   | -- dependencies
   | -- processors
   | -- test
   | -- main.py
   | -- requirements.txt
```

> For Local Setup

- Install Python
- Install Java(JRE)
- Install IDE(of your choice)
- Download SPARK binaries

> Setup Steps

- Clone the project and open it in the IDE
- Install following python libraries
    - pyspark
    - pytest
    - pytest-mock
- Set following environment variable
    - SPARK_HOME: <spark_binaries>/spark_3/
    - JAVA_HOME: <JRE_installation_path>/home/
    - env: int
- Execute the main.py

> Unit Test Setup

- Execute the test_main.py after setting the environment variables (set `env` as `TEST`) as explained above