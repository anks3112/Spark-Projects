from contextlib import nullcontext

import pytest

from spark_projects.processors.data_processor import process_data


def test_process_data(spark_fixture):
    result = process_data(spark_fixture, 'spark_projects/tests/data/', '')
    assert 'col' in result.columns
    assert result.count() == 13


def test_process_data_1(spark_fixture):
    with pytest.raises(Exception) as exec_info:
        process_data(spark_fixture, 'tests/dat/', '')
    assert str(exec_info.value) == 'Something wrong !'


@pytest.mark.parametrize('loc,expectation',
                         [('spark_projects/tests/dat/', pytest.raises(Exception, match='Something.*')),
                          ('spark_projects/tests/data/', nullcontext())], ids=['fail', 'pass'])
def test_process_data_2(spark_fixture, loc, expectation):
    with expectation:
        result = process_data(spark_fixture, loc, '')
        assert result.count() == 13
        assert 'col' in result.columns


def test_mock(spark_fixture, mocker):
    mocker.patch('spark_projects.processors.data_processor.read_data',
                 return_value=spark_fixture.createDataFrame([[1]], ['a']))
    result = process_data(spark_fixture, '', '')
    assert 'col' in result.columns
    assert result.count() == 1
